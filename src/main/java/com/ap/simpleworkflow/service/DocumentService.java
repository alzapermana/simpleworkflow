package com.ap.simpleworkflow.service;

import com.ap.simpleworkflow.Repository.DocumentRepository;
import com.ap.simpleworkflow.model.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    public void saveDocument(Document document) {
        documentRepository.save(document);
    }

    public Document getDocumentById(Long id) {
        return documentRepository.findById(id).get();
    }

    public List<Document> getDocuments() {
        return documentRepository.findAll();
    }
}
