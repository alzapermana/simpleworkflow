package com.ap.simpleworkflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleworkflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleworkflowApplication.class, args);
	}

}
