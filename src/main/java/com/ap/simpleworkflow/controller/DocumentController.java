package com.ap.simpleworkflow.controller;

import com.ap.simpleworkflow.model.Document;
import com.ap.simpleworkflow.model.User;
import com.ap.simpleworkflow.service.DocumentService;
import com.ap.simpleworkflow.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private UserService userService;

    @RequestMapping("/document")
    public String document(Authentication authentication, ModelMap modelMap) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = userService.findByUsername(userDetails.getUsername());
        modelMap.addAttribute("role", user.getRole());
        return "document_list";
    }

    @RequestMapping("/addDocument")
    public String addDocument(ModelMap modelMap) {
        modelMap.addAttribute("document", new Document());
        return "document";
    }

    @RequestMapping(value = "/saveDocument", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public ModelAndView saveDocument(Document document) {
        document.setStatus("Pending");
        documentService.saveDocument(document);
        return new ModelAndView("redirect:/document");
    }

    @RequestMapping("/viewDocument")
    public String viewDocument(@RequestParam Long id, ModelMap modelMap) {
        modelMap.addAttribute("document", documentService.getDocumentById(id));
        return "document";
    }

    @RequestMapping("/approveDocument")
    public ModelAndView approveDocument(@RequestParam Long id, @RequestParam Long role) {
        Document document = documentService.getDocumentById(id);
        document.setStatus("Approved " + role);
        documentService.saveDocument(document);
        return new ModelAndView("redirect:/document");
    }

    @RequestMapping(value = "/getDocumentList")
    public ResponseEntity<Map<String, Object>> getDocumentList() {
        Map<String, Object> map = new HashMap<>();
        map.put("data", documentService.getDocuments());
        return ResponseEntity.ok(map);
    }
}
