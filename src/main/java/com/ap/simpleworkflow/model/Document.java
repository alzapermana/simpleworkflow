package com.ap.simpleworkflow.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
public class Document {

    @Id
    @GeneratedValue
    public Long id;

    public String title;

    public String content;

    public String status;
}
